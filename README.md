# Modern C++ with templates

This project contain the following directory structure

```
.
├── solutions
└── template
    ├── cmake_template
    ├── meson_template
    └── scons_template
```

## Templates

The are template for the [CMake](https://cmake.org/), [Meson](https://mesonbuild.com/) and [Scons](https://scons.org/) build system. These are optional starting points, you only need to be able to compile and link a multi-file application using standard C++.

The solutions have been built against GCC and Clang, however any standard Modern C++ compiler will suffice.

## Testing

The projects are configured to work with the Modern C++ testing framework [doctest](https://github.com/onqtam/doctest).

This is optional and not a course requirement. 