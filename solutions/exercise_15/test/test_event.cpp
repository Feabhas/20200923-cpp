#include "Event.h"
#include "Room.h"
#include "Lamp.h"
#include "Named_lamp.h"

#include "doctest.h"
#include <tuple>

using namespace Home;
using namespace Time;

// Cannot really test default constructor!
// TEST_CASE("Default constructor")
// {
//     Event e{};
//     Instant on_time {12, 30};
//     e.do_action(on_time);
// }

TEST_CASE("Standard constructor")
{
    Lamp A1{ House::A, 1 };
    Lamp A2{ House::A, 2 };
    Named_lamp B1{ House::B, 1, "Master Bedroom rhs" };
    Named_lamp B2{ House::B, 2, "Master Bedroom lhs" };

    Room room{"Study"};

    room.add(A1);
    room.add(A2);
    room.add(B1);
    room.add(B2);

    Instant on_time {12, 30};
    Instant off_time{12, 34};
    Instant no_time {12, 32};

    Event e{on_time, off_time, room };

    e.do_action(on_time);
    CHECK(A1.is_on()==true);
    CHECK(A2.is_on()==true);
    CHECK(B1.is_on()==true);
    CHECK(B2.is_on()==true);

    e.do_action(no_time);       // should have no effect
    CHECK(A1.is_on()==true);
    CHECK(A2.is_on()==true);
    CHECK(B1.is_on()==true);
    CHECK(B2.is_on()==true);

    e.do_action(off_time);
    CHECK(A1.is_on()==false);
    CHECK(A2.is_on()==false);
    CHECK(B1.is_on()==false);
    CHECK(B2.is_on()==false);

    e.do_action(no_time);       // should have no effect
    CHECK(A1.is_on()==false);
    CHECK(A2.is_on()==false);
    CHECK(B1.is_on()==false);
    CHECK(B2.is_on()==false);
}
