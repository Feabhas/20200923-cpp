#include "doctest.h"
#include "Lamp.h"
#include "Named_lamp.h"
using namespace Home;

#include <array>


TEST_CASE("Default Constructor")
{
    Named_lamp lamp{ };
    CHECK(lamp.is_on() == false);

    Device actual = lamp.id();
    CHECK(actual.first == House::INVALID);
    CHECK(actual.second == 0);
}


TEST_CASE("Single Named Lamp usage")
{
    Named_lamp desk{ House::A, 1, "desk" };
    CHECK(desk.is_on() == false);

    SUBCASE("ID check")
    {
        Device actual = desk.id();
        CHECK(actual.first == House::A);
        CHECK(actual.second == 1);
    }

    SUBCASE("Turn Lamp On")
    {
        desk.on();
        CHECK(desk.is_on() == true);
    }

    SUBCASE("Turn Lamp Off")
    {
        desk.off();
        CHECK(desk.is_on() == false);
    }

}
