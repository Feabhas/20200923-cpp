// -----------------------------------------------------------------------------
// Module.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Module.h"
#include <iostream>

using std::cout;
using std::endl;

namespace Home
{
  Module::Module(House house, Unit unit) : device{ house, unit } {}

  Module::~Module() { switch_off(); }

  void Module::switch_on()
  {
    state = true;
    status();
  }

  void Module::switch_off()
  {
    state = false;
    status();
  }

  bool Module::is_on() const { return state; }

  void Module::status() const
  {
    cout << "(";

    // If the code is 'empty' (invalid) output dashes
    //
    cout << static_cast<char>(((device.first != House::INVALID) ?
                                 static_cast<char>(device.first) + 0x40 :
                                 '-'));
    cout << static_cast<char>((device.second != 0) ? device.second + 0x30 :
                                                     '-');

    cout << ")";

    cout << " is " << (state ? "on" : "off");
    cout << endl;
  }

  void Module::set_id(const Device& dev) { device = dev; }

  Device Module::id() const { return device; }

} // namespace Home
