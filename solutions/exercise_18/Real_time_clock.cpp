// -----------------------------------------------------------------------------
// Real_time_clock.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Real_time_clock.h"
#include "Room_controller.h"
#include <chrono>
#include <iostream>
#include <thread>

using std::this_thread::sleep_for;
using namespace std::chrono;
using namespace std::chrono_literals;
using Home::Room_controller;
using std::cout;
using std::endl;
using Time::Instant;

namespace Time
{
  bool Real_time_clock::run()
  {
    cout << current_time << endl;

    if (controller) controller->update_time(current_time);
    current_time.incr_minutes();

    sleep_for(100ms);

    // Terminate after midnight (we must send
    // the midnight time update to terminate
    // the controller)
    //
    return (current_time == Instant{ 00, 01 });
  }

} // namespace Time
