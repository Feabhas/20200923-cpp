// -----------------------------------------------------------------------------
// MessageQueue.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef MESSAGEQUEUE_H_
#define MESSAGEQUEUE_H_

#include "Instant.h"
#include <optional>
#include <queue>

namespace Home
{
  template<typename Ty>
  class Message_queue {
  public:
    void push(const Ty& in_val) { queue.push(in_val); }

    std::optional<Ty> get()
    {
      if (queue.empty()) return std::nullopt;
      Ty result{ queue.front() };
      queue.pop();
      return result;
    }

  private:
    std::queue<Ty> queue{};
  };

} // namespace Home

#endif // MESSAGEQUEUE_H_
