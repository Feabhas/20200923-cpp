// -----------------------------------------------------------------------------
// EventList.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "EventList.h"
#include "Instant.h"
#include "Room.h"
#include <algorithm>

using Home::Room;
using std::begin;
using std::end;
using std::remove_if;

namespace Time
{
  bool EventList::add_event(const Instant& on,
                            const Instant& off,
                            Home::Room&    room)
  {
    events.emplace_back(new Event{ on, off, room });

    return true;
  }

  void EventList::remove_room(const Home::Room& room)
  {
    auto it =
      remove_if(begin(events), end(events), [&room](const EventPtr& event) {
        return (&(event->get_room()) == &room);
      });

    events.erase(it, end(events));
  }

  void EventList::update_time(const Instant& time)
  {
    for (auto& event : events) {
      if (event) { event->do_action(time); }
    }
  }

} // namespace Time
