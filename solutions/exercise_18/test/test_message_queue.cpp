#include "doctest.h"

#include "Message_queue.h"
using namespace Home;

#include "Instant.h"

TEST_CASE("Default Constructor")
{
  Message_queue<Time::Instant> mq{};
  CHECK(mq.get() == std::nullopt);
}

// TEST_CASE("Get on empty")
// can't do as per STL, undefined behaviour

TEST_CASE("Push on Empty")
{
  Message_queue<Time::Instant> mq{};
  mq.push(Time::Instant{ 23, 59 });

  SUBCASE("no longer empty") { CHECK(mq.get() != std::nullopt); }

  SUBCASE("back to empty")
  {
    mq.get();
    CHECK(mq.get() == std::nullopt);
  }

  SUBCASE("return equal element")
  {
    CHECK(
      (*mq.get() == Time::Instant{ 23, 59 })); // use of (( )) to force bool eval
  }
}
