#include "doctest.h"

#include "Real_time_clock.h"
using namespace Time;

#include "Room_controller.h"
using namespace Home;

TEST_CASE("Default Constructor")
{
    Real_time_clock clock { };
    CHECK(clock.run() == true);
}

TEST_CASE("Time is midnight with no events")
{
    // Create Mock version
    Room_controller controller { };

    Real_time_clock clock { };

    connect(clock, controller);

    CHECK(clock.run() == true);
}

TEST_CASE("Time is not midnight with no events")
{
    // Create Mock version
    Room_controller controller { };

    Real_time_clock clock { Instant { 23, 59 } };

    connect(clock, controller);

    CHECK(clock.run() == false);
}
