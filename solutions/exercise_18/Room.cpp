// -----------------------------------------------------------------------------
// Room.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Room.h"
#include "Lamp.h"
#include <algorithm>
#include <functional>
#include <iostream>

using namespace std;

namespace Home
{
  Room::Room()
  {
    // A room nominally holds 4 devices
    // so pre-reserve the memory to avoid
    // unnecessary allocation / copying
    //
    devices.reserve(4);
  }

  Room::Room(std::string_view str) : name{ str } { devices.reserve(4); }

  bool Room::add(Switchable& lamp)
  {
    devices.push_back(&lamp);
    return true;
  }

  void Room::all_on()
  {
    for (auto& device : devices) {
      device->on();
    }
  }

  void Room::all_off()
  {
    for (auto& device : devices) {
      device->off();
    }
  }

  bool lamp_is_on(Switchable* l) { return l->is_on(); }

  void Room::status()
  {
    if (!name.empty()) { cout << "In the room " << name << " "; }

    // You can subtract iterators, which will
    // give you the number of elements in the array
    //
    auto num_devices = devices.size();
    auto devices_on  = count_if(begin(devices), end(devices), lamp_is_on);

    cout << "there are " << devices_on << " devices on and ";
    cout << (num_devices - devices_on) << " devices off";
    cout << endl;
  }

  void Room::set_name(std::string_view str)
  {
    name.clear();
    name = str;
  }

} // namespace Home
