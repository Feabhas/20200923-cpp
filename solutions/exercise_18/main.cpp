// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Appliance.h"
#include "Instant.h"
#include "Lamp.h"
#include "Named_lamp.h"
#include "Real_time_clock.h"
#include "Room.h"
#include "Room_controller.h"
// #include <thread>

using Home::Appliance;
using Home::Device;
using Home::House;
using Home::Lamp;
using Home::Named_lamp;
using Home::Room;
using Home::Room_controller;

using Time::Instant;
using Time::Real_time_clock;

// using std::thread;

int main()
{
  Lamp       angle_poise{ House::A, 1 };
  Lamp       ceiling{ House::D, 1 };
  Named_lamp up_light{ House::B, 2, "Up-light" };
  Appliance  fridge{ House::B, 3 };

  Room kitchen{ "Kitchen" };
  kitchen.add(angle_poise);
  kitchen.add(up_light);
  kitchen.add(fridge);

  Room bedroom{};
  bedroom.add(ceiling);

  Room_controller controller{};
  controller.add_event(Instant{ 23, 00 }, Instant{ 23, 30 }, kitchen);
  controller.add_event(Instant{ 23, 15 }, Instant{ 23, 30 }, bedroom);

  // Start the clock just before our first event
  //
  Real_time_clock clock{ Instant{ 22, 50 } };

  // Connect the clock and the controller
  //
  connect(clock, controller);

  constexpr Instant end_time{ 00, 00 };
  while (clock.the_current_time() != end_time) { // simulate concurrency
    controller.run();
    clock.run();
  }
}
