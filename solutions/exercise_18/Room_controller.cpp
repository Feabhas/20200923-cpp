// -----------------------------------------------------------------------------
// RoomController.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Room_controller.h"
#include "Instant.h"
#include "Room.h"

using Home::Room;
using Time::Instant;

namespace Home
{
  void Room_controller::add_event(const Instant& on,
                                  const Instant& off,
                                  Room&          room)
  {
    events.add_event(on, off, room);
  }

  void Room_controller::update_time(const Instant& time) { queue.push(time); }

  void Room_controller::run()
  {
    if (auto time = queue.get()) { events.update_time(*time); }
  }

} // namespace Home
