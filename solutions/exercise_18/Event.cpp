// -----------------------------------------------------------------------------
// Event.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Event.h"
#include "Room.h"

using Home::Room;

namespace Time
{
  Event::Event(const Instant& on_time,
               const Instant& off_time,
               Home::Room&    rm) :
    on{ on_time },
    off{ off_time }, room{ &rm }
  {
  }

  void Event::do_action(const Instant& time)
  {
    // This catches default-constructed
    // (essentially, invalid) Events
    //
    if (room) {
      if (time == on) room->all_on();
      if (time == off) room->all_off();
    }
  }

  const Home::Room& Event::get_room() const { return *room; }

} // namespace Time
