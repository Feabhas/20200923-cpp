// -----------------------------------------------------------------------------
// RealTimeClock.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef REALTIMECLOCK_H_
#define REALTIMECLOCK_H_

#include "Instant.h"

namespace Home
{
  class Room_controller;
}

namespace Time
{
  class Real_time_clock {
  public:
    Real_time_clock() = default;
    Real_time_clock(const Instant& start) : current_time{ start } {}

    bool    run();
    Instant the_current_time() const { return current_time; }

  private:
    Instant current_time{ 00, 00 };

    Home::Room_controller* controller{};
    void friend connect(Real_time_clock& rtc, Home::Room_controller& rc)
    {
      rtc.controller = &rc;
    }
  };

} // namespace Time

#endif // REALTIMECLOCK_H_
