// -----------------------------------------------------------------------------
// RoomController.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef ROOMCONTROLLER_H_
#define ROOMCONTROLLER_H_

#include "Event_list.h"
#include "Message_queue.h"

namespace Time
{
  class Instant;
}

namespace Home
{
  class Room;

  class Room_controller {
  public:
    void add_event(const Time::Instant& on,
                   const Time::Instant& off,
                   Home::Room&          room);
    void update_time(const Time::Instant& time);
    void run();

  private:
    Message_queue<Time::Instant> queue{};
    Time::Event_list             events{};
  };

} // namespace Home

#endif // ROOMCONTROLLER_H_
