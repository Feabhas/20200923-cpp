# Building with Scons

### To build all solutions
From the root directory
```
$ scons
```

### To build an individual solution
```
$ cd exercise_NN
$ scons
```

### To clean
```
$ scons -c
```

## Exercise SConstruct files

The individual exercise ```SConstruct``` files all have the same basic format
```
env = Environment()

# CCFLAGS : general C and C++ flags
env.Append( CCFLAGS = [
    '-Wall',
    '-Wextra',
    '-std=c++14',
    ] )

env.Program( target='exN', source=Glob('*.cpp'))
```
where ```N``` is the exercise number

If all local ```cpp``` file should note be included in the build, then the ```Glob('*.cpp')``` can be replaced by a list, as in:
```
env.Program( target='exN', source = ['main.cpp', 'other_files.cpp'])
```
or better still:
```
src_files = ['main.cpp', 'other_files.cpp']
Program(target = 'exN', source = src_files)
```
## All exercise SConstruct file
This uses a root ```SConstruct``` and then a common ```SConscript``` file in each folder