// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include "Room.h"
using namespace std;

using Home::Device;
using Home::House;
using Home::Lamp;
using Home::Room;

int main()
{
  Lamp angle_poise{ House::A, 1 };
  Lamp up_light{ House::B, 2 };
  Lamp ceiling1{ House::D, 1 };
  Lamp ceiling2{ House::D, 2 };
  Lamp ceiling3{ House::D, 3 };
  Lamp ceiling4{ House::D, 4 };

  Room kitchen{ "Kitchen" };
  kitchen.add(ceiling1);
  kitchen.add(ceiling2);
  kitchen.add(ceiling3);
  kitchen.add(ceiling4);

  Room bedroom{};
  bedroom.add(up_light);
  bedroom.add(angle_poise);

  ceiling1.on();
  kitchen.status();
  kitchen.all_on();
  kitchen.status();
  kitchen.all_off();
  kitchen.status();
}
