#include "doctest.h"
#include "Lamp.h"
using namespace Home;

#include <array>

TEST_CASE("Default Constructor")
{
    Lamp lamp{ };
    CHECK(lamp.is_on() == false);

    Device actual = lamp.id();
    CHECK(actual.first == House::INVALID);
    CHECK(actual.second == 0);
}


TEST_CASE("Single Lamp usage")
{
    Lamp desk{ House::A, 1  };
    CHECK(desk.is_on() == false);

    SUBCASE("ID check")
    {
        Device actual = desk.id();
        CHECK(actual.first == House::A);
        CHECK(actual.second == 1);
    }

    SUBCASE("Turn Lamp On")
    {
        desk.on();
        CHECK(desk.is_on() == true);
    }

    SUBCASE("Turn Lamp Off")
    {
        desk.off();
        CHECK(desk.is_on() == false);
    }

}

TEST_CASE("Change of ID")
{
    Lamp desk{ House::A, 1 };

    desk.set_id(Device{House::B, 2});

    Device actual = desk.id();
    CHECK(actual.first == House::B);
    CHECK(actual.second == 2);
}
