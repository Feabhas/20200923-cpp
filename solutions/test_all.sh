#!/bin/sh

cd ./exercise_06
scons BUILD_TYPE=tdd
./exercise_06
scons -c

cd ../exercise_07
scons BUILD_TYPE=tdd
./exercise_07
scons -c

cd ../exercise_08
scons BUILD_TYPE=tdd
./exercise_08
scons -c

cd ../exercise_09
scons BUILD_TYPE=tdd
./exercise_09
scons -c

cd ../exercise_10
scons BUILD_TYPE=tdd
./exercise_10
scons -c

cd ../exercise_11
scons BUILD_TYPE=tdd
./exercise_11
scons -c

cd ../exercise_12
scons BUILD_TYPE=tdd
./exercise_12
scons -c

cd ../exercise_13
scons BUILD_TYPE=tdd
./exercise_13
scons -c

cd ../exercise_14
scons BUILD_TYPE=tdd
./exercise_14
scons -c

cd ../exercise_15
scons BUILD_TYPE=tdd
./exercise_15
scons -c

cd ../exercise_16
scons BUILD_TYPE=tdd
./exercise_16
scons -c

cd ../exercise_17
scons BUILD_TYPE=tdd
./exercise_17
scons -c

cd ../exercise_18
scons BUILD_TYPE=tdd
./exercise_18
scons -c

cd ..