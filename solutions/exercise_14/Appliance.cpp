// -----------------------------------------------------------------------------
// Appliance.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Appliance.h"
#include <iostream>

using std::cout;

namespace Home
{
  Appliance::Appliance(House house, Unit unit) : X10_Module{ house, unit } {}

  void Appliance::on()
  {
    cout << "Appliance ";
    X10_Module::switch_on();
  }

  void Appliance::off()
  {
    cout << "Appliance ";
    X10_Module::switch_off();
  }

} // namespace Home
