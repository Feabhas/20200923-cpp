// -----------------------------------------------------------------------------
// X10_Module.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef X10_MODULE_H_
#define X10_MODULE_H_

#include "Switchable.h"
#include <cstdint>
#include <utility>

namespace Home
{
  enum class House : std::uint8_t {
    INVALID,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P
  };

  using Unit   = std::uint8_t;
  using Device = std::pair<House, Unit>;

  class X10_Module : public Switchable {
  public:
    X10_Module() = default;
    X10_Module(House house, Unit unit);
    ~X10_Module();

    void   set_id(const Device& device);
    Device id() const;

    bool is_on() const override;
    void status() const;

  protected:
    void switch_on();
    void switch_off();

  private:
    Device device{};
    bool   state{};
  };

} // namespace Home

#endif // X10_MODULE_H_
