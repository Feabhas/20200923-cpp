#include "doctest.h"
#include "Appliance.h"
using namespace Home;

#include <array>


TEST_CASE("Default Constructor")
{
    Appliance fridge{ };
    CHECK(fridge.is_on() == false);

    Device actual = fridge.id();
    CHECK(actual.first == House::INVALID);
    CHECK(actual.second == 0);
}


TEST_CASE("Single Appliance usage")
{
    Appliance fridge{ House::A, 1  };
    CHECK(fridge.is_on() == false);

    SUBCASE("ID check")
    {
        Device actual = fridge.id();
        CHECK(actual.first == House::A);
        CHECK(actual.second == 1);
    }

    SUBCASE("Turn Appliance On")
    {
        fridge.on();
        CHECK(fridge.is_on() == true);
    }

    SUBCASE("Turn Appliance Off")
    {
        fridge.off();
        CHECK(fridge.is_on() == false);
    }

}

TEST_CASE("Change of ID")
{
    Appliance fridge{ House::A, 1 };

    fridge.set_id(Device{House::B, 2});

    Device actual = fridge.id();
    CHECK(actual.first == House::B);
    CHECK(actual.second == 2);
}
