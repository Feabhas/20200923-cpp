#include "Room.h"
#include "Lamp.h"
#include "Named_lamp.h"

using namespace Home;

#include "doctest.h"

TEST_CASE("Room correct behaviour")
{
    Lamp A1{ House::A, 1 };
    Lamp A2{ House::A, 2 };
    Named_lamp B1{ House::B, 1, "Master Bedroom rhs" };
    Named_lamp B2{ House::B, 2, "Master Bedroom lhs" };

    Room room{"Study"};

    room.add(A1);
    room.add(A2);
    room.add(B1);
    room.add(B2);

    SUBCASE("All On") {
        room.all_on();

        CHECK(A1.is_on()==true);
        CHECK(A2.is_on()==true);
        CHECK(B1.is_on()==true);
        CHECK(B2.is_on()==true);
    }

    SUBCASE("All On then Off") {
        room.all_on();
        room.all_off();

        CHECK(A1.is_on()==false);
        CHECK(A2.is_on()==false);
        CHECK(B1.is_on()==false);
        CHECK(B2.is_on()==false);
    }
}

TEST_CASE("Add to many Lamps to room")
{
    Room r("Study");
    Lamp l{};
    CHECK(r.add(l)==true);
    CHECK(r.add(l)==true);
    CHECK(r.add(l)==true);
    CHECK(r.add(l)==true);
    CHECK(r.add(l)==false);
}