// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Event_list.h"
#include "Instant.h"
#include "Lamp.h"
#include "Room.h"

using Home::Device;
using Home::House;
using Home::Lamp;
using Home::Room;

using Time::Event_list;
using Time::Instant;

int main()
{
  Lamp angle_poise{ House::A, 1 };
  Lamp ceiling{ House::D, 1 };
  Lamp up_light{ House::B, 2 };

  Room kitchen{ "Kitchen" };
  kitchen.add(angle_poise);
  kitchen.add(up_light);

  Room bedroom{};
  bedroom.add(ceiling);

  Event_list events;
  events.add_event(Instant{ 00, 00 }, Instant{ 00, 10 }, kitchen);
  events.add_event(Instant{ 00, 05 }, Instant{ 00, 10 }, bedroom);

  events.update_time(Instant{ 00, 00 }); // Should turn kitchen lights on.
  events.update_time(Instant{ 00, 03 }); // Should be ignored by all events
  events.update_time(Instant{ 00, 05 }); // Should turn bedroom lights on.
  events.update_time(Instant{ 00, 10 }); // Should turn all lights off.
}
