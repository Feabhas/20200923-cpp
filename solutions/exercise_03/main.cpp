// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <cstdint> // C99
#include <iostream>
#include <string>  // std::to_string
#include <utility> // std::pair

enum class House : std::uint8_t {
  INVALID,
  A = 'A',
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  L,
  M,
  N,
  O,
  P
};

// typedef std::uint8_t Unit;  // C/C++98
using Unit = std::uint8_t; // C++11

// struct Device {
//   House first;
//   Unit  second;
// };
using Device = std::pair<House, Unit>;
struct Lamp
{
  Device device;
  bool   state;
};

// Output a Lamp's data:
//
void print_lamp(Lamp lamp)
{
  // If the code is 'empty' (invalid) output dashes
  auto first = ((lamp.device.first != House::INVALID) ?
                  static_cast<char>(lamp.device.first) :
                  '-');
  auto second =
    ((lamp.device.second != 0) ? std::to_string(lamp.device.second) : "-");

  std::cout << "Lamp(" << first << second << ")";
  std::cout << " is " << (lamp.state ? "on" : "off");
  std::cout << '\n';
}

int main()
{
  // Using initialisation list
  //
  Lamp desk_lamp{ { House::A, 1 }, false };

  // Using member-by-member initialisation
  //
  Lamp bed_main{};
  print_lamp(bed_main);

  bed_main.device.first  = House::B;
  bed_main.device.second = 1;
  bed_main.state         = true;

  print_lamp(desk_lamp);
  print_lamp(bed_main);
}
