// -----------------------------------------------------------------------------
// Lamp.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include <array>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

// -----------------------------------------------------------
// Member function definitions
//
namespace Home
{
  Lamp::Lamp(House house, Unit unit) : device{ house, unit }
  {
    cout << "Lamp::Lamp(" << static_cast<char>(house) << std::to_string(unit)
         << ")\n";
  }

  Lamp::~Lamp()
  {
    cout << "Lamp::~Lamp(" << static_cast<char>(device.first)
         << std::to_string(device.second) << ")\n";
    off();
  }

  void Lamp::on()
  {
    if (device.first == House::INVALID) return;
    this->state = true;
    status();
  }

  void Lamp::off()
  {
    if (device.first == House::INVALID) return;
    this->state = false;
    status();
  }

  bool Lamp::is_on() { return this->state; }

  void Lamp::status()
  {
    cout << "Lamp(";

    // If the code is 'empty' (invalid) output dashes
    //
    cout << static_cast<char>(((device.first != House::INVALID) ?
                                 static_cast<char>(device.first) :
                                 '-'));
    cout << ((device.second != 0) ? std::to_string(device.second) : "-");

    cout << ")";

    cout << " is " << (state ? "on" : "off");
    cout << endl;
  }

  void Lamp::set_id(const Device& dev) { device = dev; }

  Device Lamp::id() { return device; }

} // namespace Home
