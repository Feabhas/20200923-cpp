// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include <iostream>

using Home::Device;
using Home::House;
using Home::Lamp;

//#define PART_1
#define PART_2
//#define PART_3
#define PART_4

#ifdef PART_1
void lamp_status(Lamp lamp)
{
  std::cout << "The lamp is " << (lamp.is_on() ? "on" : "off") << std::endl;
}
#endif

#ifdef PART_2
void lamp_status(Lamp* const lamp)
{
  std::cout << "The lamp is " << (lamp->is_on() ? "on" : "off") << std::endl;
}
#endif

#ifdef PART_3
void lamp_status(Lamp& lamp)
{
  std::cout << "The lamp is " << (lamp.is_on() ? "on" : "off") << std::endl;
}
#endif

#ifdef PART_4
void lamp_status(const Lamp& lamp)
{
  std::cout << "The lamp is " << (lamp.is_on() ? "on" : "off") << std::endl;
}
#endif

int main()
{
  Lamp study{ House::A, 1 };
  study.on();

  lamp_status(study);
  lamp_status(&study);
}
