#include "Event_list.h"
#include "Room.h"

#include "doctest.h"

#include "Lamp.h"
#include "Named_lamp.h"

using namespace Home;
using namespace Time;

TEST_CASE("Default constructor")
{
    Lamp A1{ House::A, 1 };
    Lamp A2{ House::A, 2 };
    Named_lamp B1{ House::B, 1, "Master Bedroom rhs" };
    Named_lamp B2{ House::B, 2, "Master Bedroom lhs" };

    Room study{"Study"};
    study.add(A1);
    study.add(A2);

    Room bedroom{"Master Bedroom"};
    bedroom.add(B1);
    bedroom.add(B2);

    Instant on_time {12, 30};
    Instant off_time{12, 34};

    Event_list el;

    SUBCASE("Inital add") {
        CHECK(true==el.add_event(Instant{12, 30}, Instant{12, 34}, study));
    }

    SUBCASE("add to full") {
        for(unsigned i{} ; i < 16; ++i) {
            CHECK(true==el.add_event(Instant{12, 30}, Instant{12, 34}, study));
        }
        CHECK(false==el.add_event(Instant{12, 30}, Instant{12, 34}, study));
    }

    SUBCASE("On/Off time") {

        el.add_event(Instant{12, 30}, Instant{12, 33}, study);
        el.add_event(Instant{12, 30}, Instant{12, 34}, bedroom);

        el.update_time(Instant{12, 30});
        CHECK(A1.is_on()==true);
        CHECK(A2.is_on()==true);
        CHECK(B1.is_on()==true);
        CHECK(B2.is_on()==true);

        el.update_time(Instant{12, 31});
        CHECK(A1.is_on()==true);
        CHECK(A2.is_on()==true);
        CHECK(B1.is_on()==true);
        CHECK(B2.is_on()==true);


        el.update_time(Instant{12, 33});
        CHECK(A1.is_on()==false);
        CHECK(A2.is_on()==false);
        CHECK(B1.is_on()==true);
        CHECK(B2.is_on()==true);


        el.update_time(Instant{12, 34});
        CHECK(A1.is_on()==false);
        CHECK(A2.is_on()==false);
        CHECK(B1.is_on()==false);
        CHECK(B2.is_on()==false);

        el.update_time(Instant{12, 35});
        CHECK(A1.is_on()==false);
        CHECK(A2.is_on()==false);
        CHECK(B1.is_on()==false);
        CHECK(B2.is_on()==false);
    }

    SUBCASE("On/Off twice") {

        el.add_event(Instant{12, 30}, Instant{12, 35}, study);
        el.add_event(Instant{12, 40}, Instant{12, 45}, study);

        el.update_time(Instant{12, 30});
        CHECK(A1.is_on()==true);
        CHECK(A2.is_on()==true);

        el.update_time(Instant{12, 35});
        CHECK(A1.is_on()==false);
        CHECK(A2.is_on()==false);

        el.update_time(Instant{12, 40});
        CHECK(A1.is_on()==true);
        CHECK(A2.is_on()==true);

        el.update_time(Instant{12, 45});
        CHECK(A1.is_on()==false);
        CHECK(A2.is_on()==false);
    }
}
