#include "Lamp.h"
#include "doctest.h"
using namespace Home;

#include <array>

TEST_CASE("Single Lamp usage")
{
  Lamp desk;

  SUBCASE("Initial state") { CHECK(desk.is_on() == false); }

  SUBCASE("Turn Lamp On")
  {
    desk.on();
    CHECK(desk.is_on() == true);
  }

  SUBCASE("Turn Lamp Off")
  {
    desk.off();
    CHECK(desk.is_on() == false);
  }

  SUBCASE("ID check")
  {
    desk.set_id(Device{ House::A, 1 });
    Device actual = desk.id();
    CHECK(actual.first == House::A);
    CHECK(actual.second == 1);
  }
}
