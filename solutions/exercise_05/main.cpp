// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <algorithm>
#include <array>
#include <cstdint>
#include <iostream>
#include <utility>

enum class House : std::uint8_t {
  INVALID,
  A = 'A',
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  L,
  M,
  N,
  O,
  P
};

using Unit   = std::uint8_t;
using Device = std::pair<House, Unit>;

struct Lamp
{
  Device device;
  bool   state;
};

// Previous exercise code
void print_lamp(const Lamp& lamp);
Lamp make_lamp();
void Lamp_on(Lamp& lamp);
void Lamp_off(Lamp& lamp);
/////////////////////////////////////

constexpr unsigned sz{ 8 };

void all_lamps_on(std::array<Lamp, sz>& lamps)
{
  for (auto& lamp : lamps) {
    if (lamp.device.first != House::INVALID) { Lamp_on(lamp); }
  }
}

void all_lamps_off(std::array<Lamp, sz>& lamps)
{
  for (auto& lamp : lamps) {
    if (lamp.device.first != House::INVALID) { Lamp_off(lamp); }
  }
}

bool lamp_is_on(const Lamp& lamp)
{
  if (lamp.device.first != House::INVALID) { return lamp.state; }
  else {
    return false; // Not valid; don't count
  }
}

int main()
{
  std::array<Lamp, sz> lamps{
    Lamp{ { House::A, 1 }, false }, Lamp{ { House::A, 2 }, true },
    Lamp{ { House::A, 3 }, false }, Lamp{ { House::B, 1 }, true },
    Lamp{ { House::C, 1 }, false },
  };

  for (auto& lamp : lamps) {
    if (lamp.device.first != House::INVALID) { print_lamp(lamp); }
  }

  {
    auto lamps_on =
      std::count_if(std::begin(lamps), std::end(lamps), lamp_is_on);
    std::cout << "There are " << lamps_on << " lamps on" << std::endl;
  }
  {
    all_lamps_on(lamps);
    auto lamps_on =
      std::count_if(std::begin(lamps), std::end(lamps), lamp_is_on);
    std::cout << "There are " << lamps_on << " lamps on" << std::endl;
  }
  unsigned counter{};
  for (auto const& lamp : lamps) {
    if (lamp_is_on(lamp)) ++counter;
  }
  std::cout << "There are " << counter << " lamps on" << std::endl;
  {
    all_lamps_off(lamps);
    auto lamps_on =
      std::count_if(std::begin(lamps), std::end(lamps), [](const Lamp& lamp) {
        if (lamp.device.first == House::INVALID)
          return false; // Not valid; don't count
        return lamp.state;
      });
    std::cout << "There are " << lamps_on << " lamps on" << std::endl;
  }
}

////////////////////////////////////////////////////////////////////////////////
Lamp make_lamp()
{
  char     hs{};
  unsigned id{};

  std::cout << "House: ";
  std::cin >> hs;
  hs = std::toupper(hs);
  if ((hs < 'A') || (hs > 'P')) hs = 0;

  std::cout << "Unit:  ";
  std::cin >> id;
  if ((id > 16)) id = 0;

  return Lamp{ { static_cast<House>(hs), static_cast<Unit>(id) }, false };
}

void print_lamp(const Lamp& lamp)
{
  std::cout << "Lamp(";

  // If the code is 'empty' (invalid) output dashes
  //
  std::cout << static_cast<char>(((lamp.device.first != House::INVALID) ?
                                    static_cast<char>(lamp.device.first) :
                                    '-'));
  std::cout << ((lamp.device.second != 0) ? std::to_string(lamp.device.second) :
                                            "-");

  std::cout << ")";

  std::cout << " is " << (lamp.state ? "on" : "off");
  std::cout << std::endl;
}

void Lamp_on(Lamp& lamp)
{
  lamp.state = true;
  print_lamp(lamp);
}

void Lamp_off(Lamp& lamp)
{
  lamp.state = false;
  print_lamp(lamp);
}
