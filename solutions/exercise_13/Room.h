// -----------------------------------------------------------------------------
// Room.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef ROOM_H_
#define ROOM_H_

#include <array>
#include <string>
#include <string_view>
namespace Home
{
  class Module;

  class Room {
  public:
    Room() = default;
    Room(std::string_view str);

    bool add(Module& lamp);
    void all_on();
    void all_off();
    void status();
    void set_name(std::string_view str);

  private:
    static constexpr std::size_t sz{ 4 };
    using Container = std::array<Module*, sz>;

    Container           devices{};
    Container::iterator next{ std::begin(devices) };

    std::string name{};
  };

} // namespace Home

#endif // ROOM_H_
