#include "Lamp.h"
#include "doctest.h"
using namespace Home;

#include <array>

TEST_CASE("Single Lamp usage")
{
  Lamp A1{ { House::A, 1 }, false };

  SUBCASE("Turn Lamp On")
  {
    Lamp_on(A1);
    CHECK(A1.state == true);
  }

  SUBCASE("Turn Lamp Off")
  {
    Lamp_off(A1);
    CHECK(A1.state == false);
  }
}

TEST_CASE("Array of Lamps usage")
{
  std::array<Lamp, 8> lamps{ {
    Lamp{ { House::A, 1 }, false },
    Lamp{ { House::A, 2 }, false },
    Lamp{ { House::A, 3 }, false },
    Lamp{ { House::B, 1 }, false },
    Lamp{ { House::C, 1 }, false },
  } };

  SUBCASE("Turn Lamp On")
  {
    all_lamps_on(lamps);
    CHECK(lamps[0].state == true);
    CHECK(lamps[1].state == true);
    CHECK(lamps[2].state == true);
    CHECK(lamps[3].state == true);
    CHECK(lamps[4].state == true);
    // only first 5 are initialised
    CHECK(lamps[5].state == false);
    CHECK(lamps[6].state == false);
    CHECK(lamps[7].state == false);
  }

  SUBCASE("Turn Lamp Off")
  {
    all_lamps_off(lamps);
    for (auto& lamp : lamps) {
      CHECK(lamp.state == false);
    }
  }
}