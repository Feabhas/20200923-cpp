// -----------------------------------------------------------------------------
// Lamp.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include <array>
#include <iostream>

using std::array;
using std::cin;
using std::cout;
using std::endl;

namespace Home
{
  void print_lamp(const Lamp& lamp)
  {
    cout << "Lamp(";

    // If the code is 'empty' (invalid) output dashes
    //
    cout << ((lamp.device.first != House::INVALID) ?
               static_cast<char>(lamp.device.first) :
               '-');
    std::cout << ((lamp.device.second != 0) ?
                    std::to_string(lamp.device.second) :
                    "-");

    cout << ")";

    cout << " is " << (lamp.state ? "on" : "off");
    cout << std::endl;
  }

  void Lamp_on(Lamp& lamp)
  {
    lamp.state = true;
    print_lamp(lamp);
  }

  void Lamp_off(Lamp& lamp)
  {
    lamp.state = false;
    print_lamp(lamp);
  }



  Lamp make_lamp()
  {
    char hs{};
    int  id{};

    cout << "House: ";
    cin >> hs;
    hs -= 0x40;

    cout << "Unit:  ";
    cin >> id;

    return Lamp{ { static_cast<House>(hs), static_cast<Unit>(id) }, false };
  }

} // namespace Home
