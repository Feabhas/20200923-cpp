// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include <algorithm>
#include <array>
#include <iostream>

using Home::House;
using Home::Lamp;
using Home::make_lamp;

using namespace std;

bool lamp_is_on(const Lamp& lamp)
{
  if (lamp.device.first == House::INVALID) {
    return false; // Not valid; don't count
  }
  else {
    return lamp.state;
  }
}

constexpr unsigned sz{ 8 };

void all_lamps_on(std::array<Lamp, sz>& lamps);
void all_lamps_off(std::array<Lamp, sz>& lamps);

int main()
{
  // Using initialisation list
  //
  array<Lamp, sz> lamps{
    Lamp{ { House::A, 1 }, false }, Lamp{ { House::A, 2 }, false },
    Lamp{ { House::A, 3 }, false }, Lamp{ { House::B, 1 }, false },
    Lamp{ { House::C, 1 }, false },
  };

  lamps[5] = make_lamp();

  all_lamps_on(lamps);

  auto lamps_on = count_if(begin(lamps), end(lamps), lamp_is_on);

  cout << "There are " << lamps_on << " lamps on" << endl;
}

/////////////////////////////////////////////////
void all_lamps_on(array<Lamp, sz>& lamps)
{
  for (auto& lamp : lamps) {
    if (lamp.device.first != House::INVALID) { Lamp_on(lamp); }
  }
}

void all_lamps_off(array<Lamp, sz>& lamps)
{
  for (auto& lamp : lamps) {
    if (lamp.device.first != House::INVALID) { Lamp_off(lamp); }
  }
}