# Using Cmake

1. Create build directory 
2. Invoke CMake 
3. Invoke make 
4. Run application 

```
$ mkdir build && cd build
$ cmake ..
$ make
$ ./src/App
```
# Adding files
All `.cpp` file, except `main.cpp` are added to a local library. This has two benefits of keeping the `main()` function seperate from the rest of the project. This then aids testing.

In the file `src/CMakeLists.txt`, uncomment and modify the line:
```
add_library (proj file1.cpp)   
```
to add any `.cpp` file required as part of the project build.

Also Uncomment the line:
```
# target_link_libraries (App proj) # uncomment when files added to library
```

# Tesing with doctest

Ensure, in the file `src/CMakeList.txt`, source file for testing, have been added to the library, as directed above.

In the file, `test\CMakeLists.txt`, uncomment the directed lines:
```
# uncomment when files in src directory
# target_link_libraries (Test proj )
```
e.g.
```
# uncomment when files in src directory
target_link_libraries (Test proj)
```
## Running the tests

Running `make tset` will build and run the test executable.

The `doctest` output can be seen by running the test executable directly, e.g.
```
./testTest`
```
